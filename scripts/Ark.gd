extends Node2D

var workerToTransfert

## Stats
var heat = 50
var food = 50
var petroleum = 50
var water = 50

var consume = 2
var production = 5

var piloted = false

signal dead
signal engine_broken
signal engine_repaired

func _ready():
	$Rooms/Cockpit.assignWorker(false)
	$Rooms/Cockpit.worker.connect("selected", self, "_on_Worker_seleted")

	$Rooms/EngineRoom.assignWorker(false)
	$Rooms/EngineRoom.worker.connect("selected", self, "_on_Worker_seleted")

	for room in $Rooms.get_children():
		room.connect("selected", self, "_on_Room_selected")

func _process(delta):
	# Increase intern resources
	var localConsume = consume * delta
	var localProduction = production * delta
	if(is_instance_valid($Rooms/BoilerRoom.worker)) && petroleum > 0 && water > 0:
		petroleum = clamp(petroleum - localConsume, 0, 100)
		water = clamp(water - localConsume, 0, 100)
		heat = clamp(heat + localProduction, 0, 100)
	if(is_instance_valid($Rooms/GreenHouse.worker)) && water > 0:
		water = clamp(water - localConsume, 0, 100)
		food = clamp(food + localProduction, 0, 100)
	if(is_instance_valid($Rooms/EngineRoom.worker)):
		$Rooms/EngineRoom/Engine.startFixing()
	else:
		$Rooms/EngineRoom/Engine.stopFixing()

	petroleum = clamp(petroleum - localConsume, 0, 100)

	piloted = false
	if(is_instance_valid($Rooms/Cockpit.worker)):
		piloted = true

	# Decrease intern resources
	heat = clamp(heat - localConsume, 0, 100)
	food = clamp(food - localConsume, 0, 100)

	refreshHUD()

	if(heat < 20):
		$Frost.visible = true
		if (! $SoundFrost.playing):
			$SoundFrost.play()
	else:
		$Frost.visible = false
		$SoundFrost.stop()

	if(heat <= 0 || food <= 0 ):
		emit_signal("dead")

func _on_Worker_seleted(worker):
	workerToTransfert = worker

func _on_Room_selected(room):
	if(! is_instance_valid(room.worker) && is_instance_valid(workerToTransfert)):
		workerToTransfert.queue_free()
		room.assignWorker()
		room.worker.connect("selected", self, "_on_Worker_seleted")

func refreshHUD():
	$HUD.get_node("Heat").value = heat
	$HUD.get_node("Food").value = food
	$HUD.get_node("Petroleum").value = petroleum
	$HUD.get_node("Water").value = water

func refillWater(amount):
	water = clamp(water + amount, 0, 100)
	
func refillPetroleum(amount):
	petroleum = clamp(petroleum + amount, 0, 100)


func _on_Engine_broken():
	emit_signal("engine_broken")
	$Shaders/Moteur.emitting = false
	$BrokenEngine.visible = true

func _on_Engine_repair():
	emit_signal("engine_repaired")
	$Shaders/Moteur.emitting = true
	$BrokenEngine.visible = false

func setScore(score):
	$HUD/Score.text = "Score: " + str(score)

func isPiloted():
	return piloted

func _on_SoundControl_control_music(music):
	if(music):
		$AudioStreamPlayer.play()
	else:
		$AudioStreamPlayer.stop()
