extends Node2D

var Worker = load("res://scenes/Worker.tscn")
var worker

signal selected

func _on_ColorRect_gui_input(event):
	if(event.get_class() == "InputEventMouseButton" && event.pressed):
		emit_signal("selected", self)

func assignWorker(withSound = true):
	worker = Worker.instance()
	worker.position = $WorkerSpawn.position
	worker.position.y -= 38
	worker.z_index = 10
	worker.selectAnimationFor(get_name())
	if(withSound):
		worker.playArrivalSound()
	add_child(worker)
	
func leaveWorker():
	worker.queue_free()
