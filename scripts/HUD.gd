extends Node2D


func _on_Heat_mouse_entered():
	$HeatLabel.visible = true
	$HeatTip.visible = true

func _on_Heat_mouse_exited():
	$HeatLabel.visible = false
	$HeatTip.visible = false

func _on_Food_mouse_entered():
	$FoodLabel.visible = true
	$FoodTip.visible = true

func _on_Food_mouse_exited():
	$FoodLabel.visible = false
	$FoodTip.visible = false

func _on_Petroleum_mouse_entered():
	$PetroleumLabel.visible = true
	$CollectTip.visible = true

func _on_Petroleum_mouse_exited():
	$PetroleumLabel.visible = false
	$CollectTip.visible = false

func _on_Water_mouse_entered():
	$WaterLabel.visible = true
	$CollectTip.visible = true

func _on_Water_mouse_exited():
	$WaterLabel.visible = false
	$CollectTip.visible = false
