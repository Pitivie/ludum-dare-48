extends Control

var _music_list = (["res://music/BGM/"])
var song = _music_list[randi() % _music_list.size()]

func _ready():

	$BGM.stream = load(song)
	$BGM.play()

