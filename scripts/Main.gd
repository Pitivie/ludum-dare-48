extends Node2D

const SPEED = 2
var currentSpeed = SPEED
var slow = 0

var Map = load("res://scenes/Map.tscn")
var GameOver = load("res://scenes/GameOver.tscn")
var currentChunck
var nextChunck
var previousChunck
var nextRightChunck

var drillHeight = 9

var engineBroken = false

var score = 0
var coefResource = 0.36

func _ready():
	currentChunck = Map.instance()
	currentChunck.connect("mapEnded", self, "_on_GameMap_mapEnded")
	currentChunck.connect("nearMapEnded", self, "_on_GameMap_nearMapEnded")
	add_child(currentChunck)

	$Arch.position.x += (currentChunck.WIDTH/2 - drillHeight) * 32 + 16
	$Camera2D.position.x += (currentChunck.WIDTH/2 - drillHeight) * 32 + 16

func _physics_process(_delta):
	if(! has_node("Arch")):
		return

	if(engineBroken):
		currentSpeed = 0

	if(!engineBroken && $Arch.petroleum == 0):
		currentSpeed = float(currentSpeed) / 3

	var enableControl = true
	if(!engineBroken && !$Arch.isPiloted()):
		currentSpeed = (float(currentSpeed) / 3) * 2
		enableControl = false

	$Arch.position.y += currentSpeed
	$Camera2D.position.y += currentSpeed
	
	if (enableControl && currentSpeed > 0 && Input.is_action_pressed("ui_right")):
		$Arch.position.x += 1
		$Camera2D.position.x += 1

	if (enableControl && currentSpeed > 0 && Input.is_action_pressed("ui_left")):
		$Arch.position.x -= 1
		$Camera2D.position.x -= 1

	var posX = $Arch.position.x + $Arch/Shape.position.x
	var posY = $Arch.position.y + $Arch/Shape.position.y + 50
	if(posY > endOfCurrentChucnk() && is_instance_valid(nextChunck)):
		previousChunck = currentChunck
		changeChunch()
	var tileMap = currentChunck.get_node("TileMap")
	var tile_pos = tileMap.world_to_map(Vector2(posX, posY))
	
	# Find the colliding tile position
	tile_pos.y = int(tile_pos.y) % 50
	tile_pos.x -= currentChunck.WIDTH / 2
	tile_pos.y -= currentChunck.HEIGHT / 2

	var minDrill = tile_pos.x - ((drillHeight) / 2)
	var normal = tile_pos.y
	for number in range(drillHeight):
		tile_pos.x = minDrill + number
		tile_pos.y = normal

		if(number == 2 || number == 6 || number == 9):
			tile_pos.y = normal - 1 
		if(number == 4):
			tile_pos.y = normal + 1 

		var tile_id = tileMap.get_cellv(tile_pos)
		if(tile_id == 1): #Rock
			slow = $Arch.position.y + 32
		elif(tile_id == 2): #Water
			$Arch.refillWater(2)
		elif(tile_id == 3): #Petroleum
			$Arch.refillPetroleum(2.5)
		tileMap.set_cellv(tile_pos, 4)

	if($Arch.position.y < slow):
		currentSpeed = float(SPEED) / 2
	else:
		currentSpeed = SPEED

	score = int($Arch.position.y)
	$Arch.setScore(score)

func changeChunch():
	currentChunck = nextChunck
	nextChunck = null

func endOfCurrentChucnk():
	return currentChunck.getHeight() + currentChunck.position.y

func _on_GameMap_mapEnded():
	previousChunck.queue_free()

func _on_GameMap_nearMapEnded():
	addMapChunck()

func addMapChunck():
	coefResource += 0.01
	nextChunck = Map.instance()
	nextChunck.coef = coefResource
	nextChunck.position.y = endOfCurrentChucnk()
	nextChunck.connect("mapEnded", self, "_on_GameMap_mapEnded")
	nextChunck.connect("nearMapEnded", self, "_on_GameMap_nearMapEnded")
	add_child(nextChunck)

func _on_Arch_dead():
	$Arch.queue_free()
	currentChunck.queue_free()
	var gameOver = GameOver.instance()
	gameOver.score = score
	gameOver.position = $Camera2D.position
	gameOver.position.x -= 250
	gameOver.position.y -= 360

	add_child(gameOver)

func _on_Arch_engine_broken():
	engineBroken = true

func _on_Arch_engine_repaired():
	engineBroken = false
