extends Node2D

const WIDTH = 50
const HEIGHT = 50

const TILES = {
	'Dirt': 0,
	'Rock': 1,
	'Water': 2,
	'Petroleum': 3
}
var coef = 0.3

var openSimplexNoise
var openSimplexNoiseRessources

signal nearMapEnded
signal mapEnded

func _ready():
	randomize()
	openSimplexNoise = OpenSimplexNoise.new()
	openSimplexNoise.seed = randi()
	openSimplexNoiseRessources = OpenSimplexNoise.new()
	openSimplexNoiseRessources.seed = randi()
	
	
	openSimplexNoise.octaves = 4
	openSimplexNoise.period = 15
	openSimplexNoise.lacunarity = 1.5
	openSimplexNoise.persistence = 0.75
	
	openSimplexNoiseRessources.octaves = 9
	openSimplexNoiseRessources.period = 8
	openSimplexNoiseRessources.lacunarity = 1.0
	openSimplexNoiseRessources.persistence = 0.5
	
	_generate_world()
	_generate_ressources()
	
	$TileMap.position.x = (WIDTH * $TileMap.cell_size.x) / 2
	$TileMap.position.y = (HEIGHT * $TileMap.cell_size.y) / 2
	
	$nearEndOfMap.rect = Rect2(0, 0, getWidth(), 20)
	$endOfMap.rect = Rect2(0, 0, getWidth(), 20)

	$nearEndOfMap.position.y = getHeight() - 100
	$endOfMap.position.y = getHeight()

func _generate_world():
	for x in WIDTH:
		for y in HEIGHT:
			$TileMap.set_cellv(Vector2(x - WIDTH / 2, y - HEIGHT / 2), _get_tile_index(openSimplexNoise.get_noise_2d(float(x), float(y))))
		$TileMap.update_bitmask_region()

func _generate_ressources():
	for x in WIDTH:
		for y in HEIGHT:
			var tilePosition = Vector2(x - WIDTH / 2, y - HEIGHT / 2)
			var currentTile = $TileMap.get_cellv(tilePosition)
			$TileMap.set_cellv(tilePosition, _get_ressources_index(openSimplexNoiseRessources.get_noise_2d(float(x), float(y)), currentTile))
		$TileMap.update_bitmask_region()


func _get_tile_index(noise_sample):
	if noise_sample < -0.2:
		return TILES.Rock
	return TILES.Dirt


func _get_ressources_index(noise_sample_ressources, currentTile):
	if noise_sample_ressources <-coef:
		return TILES.Petroleum
	if noise_sample_ressources >coef:
		return TILES.Water
	return currentTile

func getHeight():
	return HEIGHT * $TileMap.cell_size.y

func getWidth():
	return WIDTH * $TileMap.cell_size.x

func _on_nearEndOfMap_screen_entered():
	emit_signal("nearMapEnded")

func _on_endOfMap_screen_exited():
	emit_signal("mapEnded")
