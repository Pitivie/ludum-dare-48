extends Node2D

signal selected
var selected = false

var sounds = ["Worker02", "Worker03"]

func _on_front_gui_input(event):
	if(event.get_class() == "InputEventMouseButton" && event.pressed && event.button_index == 1):
		selected = ! selected
		if (selected):
			$selected.visible = true
			$Worker01.play()
		else:
			$selected.visible = false
		emit_signal("selected", self)

func playArrivalSound():
	var sound = sounds[randi() % sounds.size()]
	get_node(sound).play()

func selectAnimationFor(roomName):
	if(roomName == "Cockpit"):
		$"Pilot worker".visible = true
	if(roomName == "EngineRoom"):
		$"engine's worker".visible = true
	if(roomName == "GreenHouse"):
		$"Greenhouse worker".visible = true
	if(roomName == "BoilerRoom"):
		$"Boiler's worker".visible = true
