extends Node2D

var Tutorial = preload("res://scenes/Tutorial.tscn")
var Credit = preload("res://scenes/Crédit.tscn")
var Main = preload("res://scenes/Main.tscn")

var tuto = 0

func _on_Credit_pressed():
	var credit = Credit.instance()
	credit.connect("back", self, "_on_Back_pressed")
	add_child(credit)
	$Buttons.visible = false

func _on_Tuto_pressed():
	var tutorial = Tutorial.instance()
	tutorial.connect("back", self, "_on_Back_pressed")
	add_child(tutorial)
	$Buttons.visible = false

func _on_Back_pressed():
	$Buttons.visible = true

func _on_Start_pressed():
	$Buttons.visible = false
	$next.visible = true
	$tuto1.visible = true

func _on_next_pressed():
	if($tuto2.visible == true):
		$AudioStreamPlayer.stop()
		var main = Main.instance()
		add_child(main)
	if($tuto1.visible == true):
		$tuto2.visible = true
