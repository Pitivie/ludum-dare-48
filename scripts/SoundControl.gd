extends Node2D

var music = true

signal control_music

func _ready():
	update()

func update():
	$On.visible = false
	$Off.visible = false
	if(music):
		$On.visible = true
	else:
		$Off.visible = true

func _on_Control_gui_input(event):
	if(event.get_class() == "InputEventMouseButton" && event.pressed):
		music = ! music
		update()
		emit_signal("control_music", music)
