extends Node2D

var repairing = 100
var fix = false

signal broken
signal repair

# Called when the node enters the scene tree for the first time.
func _process(_delta):
	if(repairing == 0):
		$Fixing.visible = true

	if (fix && repairing < 100):
		repairing = clamp(repairing + 1, 0, 100)

	if (repairing == 100 && $Fixing.visible):
		emit_signal("repair")

	if (repairing == 100):
		#$SoundEngine.play()
		$Fixing.visible = false

	$Fixing.value = repairing

func _on_Timer_timeout():
	if(randi() % 100 > 75 && repairing == 100):
		repairing = 0
		$SoundAlarm.play()
		emit_signal("broken")

func startFixing():
	fix = true

func stopFixing():
	fix = false
